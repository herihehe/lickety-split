"use strict";

// import Angular 2
import {Component} from "angular2/core";

// import Angular 2 Component Router
// reference: http://blog.thoughtram.io/angular/2015/06/16/routing-in-angular-2.html
import {RouteConfig, Route, RouterOutlet, RouterLink, Router} from "angular2/router";

// app components
import {Home} from "../pages/home/home";
import {BeatTheTimer} from "../pages/beat-the-timer/beat-the-timer";
import {Countdown} from "../pages/countdown/countdown";
import {Guide} from "../pages/guide/guide";
import {Language} from "../modules/language";
import {Purchase} from "../modules/purchase";

// app services
//import {appServicesInjectables} from "core/services/services";

@Component({
	selector: "app",
	templateUrl: "core/app.template.html", //template: "<router-outlet></router-outlet>",
	directives: [RouterOutlet, RouterLink]
})
@RouteConfig([
	{ path: "/", component: Home, name: "Home", data: undefined }, // the as serves as alias for links, etc
	{ path: "/beat-the-timer/...", component: BeatTheTimer, name: "BeatTheTimer", data: undefined },
	{ path: "/countdown/...", component: Countdown, name: "Countdown", data: undefined },
	{ path: "/guide/...", component: Guide, name: "Guide", data: undefined },
	{ path: "/language/...", component: Language, name: "Language", data: undefined },
	{ path: "/purchase", component: Purchase, name: "Purchase", data: undefined }
])
export class App {
	constructor() {
		// console.log("Application bootstrapped!");
	}
}
