declare var SVGInjector: any;

export class SvgService {
	constructor() {}

	injectSvg( allSvgs: any, language: string ){
		SVGInjector(allSvgs, {}, () => {
			var nodes = ["image"];

			nodes.forEach( selector => {
				var node = document.querySelectorAll(selector);
				if(node.length){
					Object.keys(node).forEach( i => {
						var dom = node[i],
							url = dom.getAttribute("xlink:href");
							
						if( url.indexOf("data") === -1 ){
							dom.setAttribute("xlink:href", "images/" + language + "/" + url);
						}
					});
				}
			});
		});
	}
}
