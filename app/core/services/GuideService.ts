import {Injectable} from "angular2/core";

export class Guide {
	constructor(public id: number, public content: string) { }
}

@Injectable()
export class GuideService {
	getGuides() { return guidesPromise; }
	getGuide(id: number | string) {
	return guidesPromise
		.then(guides => guides.filter(g => g.id === +id)[0]);
	}
}

var GUIDES = [
	new Guide(1, "guide.svg"),
	new Guide(2, "guide-2.svg"),
	new Guide(3, "guide-3.svg")
];

var guidesPromise = Promise.resolve(GUIDES);
