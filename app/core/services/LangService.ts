import {Observable} from "rxjs/Observable";
import "rxjs/add/operator/share";

export class LangService {
	public language$: Observable<Array<string>>;
	private _languageObserver: any;
	private _language: string;

	constructor() {
		this._language = "en";
		this.language$ = new Observable(observer => {
			this._languageObserver = observer;
		}).share();
	}
	change(value: string) {
		this._language = value;
		this._languageObserver.next(this._language);
	}
	load() {
		this._languageObserver.next(this._language);
	}
}
