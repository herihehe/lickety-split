import {Observable} from "rxjs/Observable";
import "rxjs/add/operator/share";

export class PremiumService {
	public premium$: Observable<Array<string>>;
	private _premiumObserver: any;
	private _premium: boolean;

	constructor() {
		this._premium = false;
		this.premium$ = new Observable(observer => {
			this._premiumObserver = observer;
		}).share();
	}
	
	purchase(type) {
		this._premium = type;
		this._premiumObserver.next(this._premium);
	}

	load() {
		this._premiumObserver.next(this._premium);
	}
}
