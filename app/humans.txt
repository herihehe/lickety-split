# humanstxt.org/
# The humans responsible & technology colophon

# TEAM

  Heri Setiawan -- Heri Setiawan -- http://www.kedaipixel.com

# THANKS

  Thanks for all the fish

# TECHNOLOGY COLOPHON

  HTML5, CSS3
  Angular 2
  TypeScript
  ES2015
  JSPM
  SystemJS
  NodeJS
  NPM
  Gulp
