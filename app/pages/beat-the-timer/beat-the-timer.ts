"use strict";

// import Angular 2
import {Component} from "angular2/core";
import {RouteConfig, Route, RouterOutlet, RouterLink, Router} from "angular2/router";

// Timer apps
import {BeatTheTimerIndex} from "../beat-the-timer/beat-the-timer-index";
import {BeatTheTimerPlay} from "../beat-the-timer/play";
import {BeatTheTimerCustomTime} from "../beat-the-timer/custom-time";

@Component({
	selector: "page-beat-the-timer",
	templateUrl: "pages/beat-the-timer/beat-the-timer.template.html",
	directives: [RouterOutlet, RouterLink]
})
@RouteConfig([
	{ path: "/", component: BeatTheTimerIndex, name: "BeatTheTimerIndex", useAsDefault: true },
	{ path: "/play", component: BeatTheTimerPlay, name: "BeatTheTimerPlay" },
	{ path: "/custom-time", component: BeatTheTimerCustomTime, name: "BeatTheTimerCustomTime" }
])
export class BeatTheTimer {
	constructor() {}
}
