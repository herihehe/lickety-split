"use strict";

// import Angular 2
import {Component} from "angular2/core";
import {NgClass} from "angular2/common";
import {RouteConfig, Route, RouterOutlet, RouterLink, Router} from "angular2/router";
import {Menu} from "../../modules/menu";
import {LangService} from "../../core/services/LangService";

@Component({
	selector: "page-btt-custom-time",
	templateUrl: "pages/beat-the-timer/custom-time.template.html",
	directives: [RouterOutlet, RouterLink, NgClass, Menu]
})

export class BeatTheTimerCustomTime {
	private language: string = "en";
	done:Boolean;
	menus:Array<any>;
	constructor( private _LangService: LangService) {
		this.done = false;
		this.menus = [
			{
				path: ["../", "BeatTheTimerPlay"],
				premium: true,
				button: "button-custom-1.svg"
			},
			{
				path: ["../", "BeatTheTimerPlay"],
				premium: true,
				button: "button-custom-2.svg"
			},
			{
				path:["../", "BeatTheTimerPlay"],
				premium: true,
				button: "button-custom-3.svg"
			},
			{
				path: ["../", "BeatTheTimerPlay"],
				premium: true,
				button: "button-custom-4.svg"
			},
			{
				path: ["../", "BeatTheTimerPlay"],
				premium: true,
				button: "button-custom-5.svg"
			},
			{
				path: ["../", "BeatTheTimerPlay"],
				premium: true,
				button: "button-custom-10.svg"
			}
		];
	}

	ngOnInit(){
		this._LangService.language$.subscribe(
			currentLanguage => {
				this.language = currentLanguage;
		});
		this._LangService.load();
	}

	back(){
		window.history.back();
	}
}
