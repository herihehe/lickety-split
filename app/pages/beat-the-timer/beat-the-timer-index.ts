"use strict";

// import Angular 2
import {Component} from "angular2/core";
import {RouteConfig, Route, RouterOutlet, RouterLink, Router} from "angular2/router";
import {Menu} from "../../modules/menu";
import {LangService} from "../../core/services/LangService";

@Component({
	selector: "page-beat-the-timer-index",
	templateUrl: "pages/beat-the-timer/beat-the-timer-index.template.html",
	directives: [RouterOutlet, RouterLink, Menu]
})

export class BeatTheTimerIndex {
	private language: string = "en";
	menus:Array<any>;
	constructor(private _langService: LangService) {
		this.menus = [
			{
				path: ["../", "BeatTheTimerPlay"],
				premium: true,
				button: "button-ready-to-go.svg"
			},
			{
				path: ["../", "BeatTheTimerPlay"],
				premium: true,
				button: "button-make-bed.svg"
			},
			{
				path:["../", "BeatTheTimerPlay"],
				premium: true,
				button: "button-get-dressed.svg"
			},
			{
				path: ["../", "BeatTheTimerPlay"],
				premium: false,
				button: "button-clean-room.svg"
			},
			{
				path: ["../", "BeatTheTimerPlay"],
				premium: false,
				button: "button-pjs-on.svg"
			},
			{
				path: ["../", "BeatTheTimerPlay"],
				premium: true,
				button: "button-meal-time.svg"
			},
			{
				path: ["../", "BeatTheTimerCustomTime"],
				premium: true,
				button: "button-custom-time.svg"
			},
			{
				path: ["../", "BeatTheTimerPlay"],
				premium: true,
				button: "button-change-song.svg"
			}
		];
	}

	ngOnInit() {
		this._langService.language$.subscribe(
			currentLanguage => {
				this.language = currentLanguage;
		});
		this._langService.load();
	}
}
