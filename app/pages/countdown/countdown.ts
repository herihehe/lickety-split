"use strict";

// import Angular 2
import {Component} from "angular2/core";
import {RouteConfig, Route, RouterOutlet, RouterLink, Router} from "angular2/router";

// Timer apps
import {CountdownIndex} from "../countdown/countdown-index";
import {CountdownPlay} from "../countdown/play";
import {CountdownCustomTime} from "../countdown/custom-time";

@Component({
	selector: "page-countdown",
	templateUrl: "pages/countdown/countdown.template.html",
	directives: [RouterOutlet, RouterLink]
})
@RouteConfig([
	{ path: "/", component: CountdownIndex, name: "CountdownIndex", useAsDefault: true },
	{ path: "/play", component: CountdownPlay, name: "CountdownPlay" },
	{ path: "/custom-time", component: CountdownCustomTime, name: "CountdownCustomTime" }
])
export class Countdown {

	constructor() {
		// console.log("Countdown component loaded");
	}
}
