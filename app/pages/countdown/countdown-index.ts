"use strict";

// import Angular 2
import {Component} from "angular2/core";
import {RouteConfig, Route, RouterOutlet, RouterLink, Router} from "angular2/router";
import {Menu} from "../../modules/menu";
import {LangService} from "../../core/services/LangService";

@Component({
	selector: "page-countdown-index",
	templateUrl: "pages/countdown/countdown-index.template.html",
	directives: [RouterOutlet, RouterLink, Menu]
})
export class CountdownIndex {
	private language: string = "en";
	menus:Array<any>;
	constructor(
		private _langService: LangService) {
		this.menus = [
			{
				path: ["../", "CountdownPlay"],
				premium: false,
				button: "button-brush-teeth.svg"
			},
			{
				path: ["../", "CountdownPlay"],
				premium: false,
				button: "button-take-turn.svg"
			},
			{
				path: ["../", "CountdownPlay"],
				premium: true,
				button: "button-reading-time.svg"
			},
			{
				path: ["../", "CountdownPlay"],
				premium: true,
				button: "button-study-time.svg"
			},
			{
				path: ["../", "CountdownCustomTime"],
				premium: true,
				button: "button-custom-time-countdown.svg"
			},
			{
				path: ["../", "CountdownPlay"],
				premium: true,
				button: "button-change-song-countdown.svg"
			}
		];
	}

	ngOnInit() {
		this._langService.language$.subscribe(
			currentLanguage => {
				this.language = currentLanguage;
		});
		this._langService.load();
	}
}
