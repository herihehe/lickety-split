"use strict";

// import Angular 2
import {Component} from "angular2/core";
import {NgClass} from "angular2/common";
import {RouteConfig, Route, RouterOutlet, RouterLink, Router} from "angular2/router";
import {LangService} from "../../core/services/LangService";

@Component({
	selector: "page-btt-play",
	templateUrl: "pages/countdown/play.template.html",
	directives: [RouterOutlet, RouterLink, NgClass]
})

export class CountdownPlay {
	private language: string = "en";
	done:Boolean;
	constructor( private _LangService: LangService) {
		this.done = false;
	}

	ngOnInit(){
		this._LangService.language$.subscribe(
			currentLanguage => {
				this.language = currentLanguage;
		});
		this._LangService.load();
	}
}