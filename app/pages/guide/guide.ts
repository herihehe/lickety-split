"use strict";

// import Angular 2
import {Component} from "angular2/core";
import {RouteConfig, Route, RouterOutlet, RouterLink, Router, ROUTER_DIRECTIVES} from "angular2/router";

// Guide components
import {GuideDetail} from "../guide/guide-detail";
import {GuideService} from "../../core/services/GuideService";

@Component({
	selector: "page-guide",
	templateUrl: "pages/guide/guide.template.html",
	providers:  [GuideService],
	directives: [ROUTER_DIRECTIVES]
})

@RouteConfig([
	{path:"/detail/:id", name: "GuideDetail", component: GuideDetail}
])

export class Guide {
	constructor() {
		// console.log("guide component loaded");
	}
}
