import {Component, OnInit}  from "angular2/core";
import {RouteParams, Router, RouterLink} from "angular2/router";
import {Guide, GuideService}   from "../../core/services/GuideService";
import {LangService} from "../../core/services/LangService";

@Component({
	templateUrl : "pages/guide/guide-detail.template.html",
	directives: [RouterLink]
})

export class GuideDetail implements OnInit {
	guide: Guide;
	private language: string = "en";
	total: any;

	constructor(
	private _router:Router,
	private _routeParams:RouteParams,
	private _service:GuideService,
	private _langService: LangService) {}

	ngOnInit() {
		this._langService.language$.subscribe(
			currentLanguage => {
				this.language = currentLanguage;
		});
		this._langService.load();

		let id = this._routeParams.get("id");
		this.total = this._service.getGuides();
		this.total = this.total._result.length;
		this._service.getGuide(id).then(guide => {
			setTimeout( () => {
				this.guide = guide;
			});
		});
	}
}
