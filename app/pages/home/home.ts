"use strict";

// import Angular 2
import {Component, ElementRef, Inject, OnInit} from "angular2/core";
import {RouteConfig, Route, RouterOutlet, RouterLink, Router} from "angular2/router";
import {LanguageIndex} from "../../modules/language-index";
import {LangService} from "../../core/services/LangService";

@Component({
	selector: "page-home",
	templateUrl: "pages/home/home.template.html",
	directives: [RouterOutlet, RouterLink, LanguageIndex]
})

export class Home implements OnInit {
	private language: string = "en";
	constructor(private _langService: LangService) {}

	ngOnInit() {
		this._langService.language$.subscribe(
			currentLanguage => {
				this.language = currentLanguage;
		});
		this._langService.load();
	}
}
