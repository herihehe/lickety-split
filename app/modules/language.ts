import {Component, View} from "angular2/core";
import {RouteConfig, Route, RouterOutlet, RouterLink, Router} from "angular2/router";

// Timer apps
import {LanguageIndex} from "../modules/language-index";
import {LanguageSelection} from "../modules/language-selection";

@Component({
	selector: "language"
})

@View({
	templateUrl: "modules/language.template.html",
	directives: [RouterOutlet, RouterLink]
})

@RouteConfig([
	{ path: "/", component: LanguageIndex, name: "LanguageIndex", useAsDefault: true },
	{ path: "/select-language", component: LanguageSelection, name: "LanguageSelection" }
])

export class Language {
	// constructor() {
	// }
}
