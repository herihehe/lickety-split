"use strict";

// import Angular 2
import {Component, View} from "angular2/core";
import {RouteConfig, Route, RouterOutlet, RouterLink, Router} from "angular2/router";
import {PremiumService} from "../core/services/PremiumService";
import {LangService} from "../core/services/LangService";

@Component({
	selector: "purchase-index"
})

@View({
	templateUrl: "modules/purchase.template.html",
	directives: [RouterOutlet, RouterLink]
})

export class Purchase {
	private language: string = "en";
	private premium: boolean = false;
	
	constructor(
		private _PremiumService: PremiumService, 
		private _LangService: LangService){}
	
	ngOnInit(){
		this._LangService.language$.subscribe( 
			currentLanguage => {
				this.language = currentLanguage;
		});

		this._LangService.load();

		this._PremiumService.premium$.subscribe( 
			premiumStatus => {
				this.premium = premiumStatus;
		});

		this._PremiumService.load();
	}

	purchase(type){
		this._PremiumService.purchase(type);
		this.back();
	}

	back(){
		window.history.back();
	}
}
