"use strict";

// import Angular 2
import {Component, View} from "angular2/core";
import {RouteConfig, Route, RouterOutlet, RouterLink, Router} from "angular2/router";
import {LangService} from "../core/services/LangService";

@Component({
	selector: "language-index"
})

@View({
	template: `
		<ul class="nav nav-lang u-cf">
			<li class="col-xs-3"><a [routerLink]="['/Language', 'LanguageSelection']" class="u-block"><img src="images/{{language}}/button-lang.svg" alt="" class="width-100p h-a image-svg"></a></li>
		</ul>
	`,
	directives: [RouterOutlet, RouterLink]
})

export class LanguageIndex {
	private language: string;

	constructor(private _langService: LangService) {
		this.language = _langService.language$;
	}

	ngOnInit() {
		this._langService.language$.subscribe(currentLanguage => {
			this.language = currentLanguage;
		});
		this._langService.load();
	}
}
