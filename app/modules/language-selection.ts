"use strict";

// import Angular 2
import {Component} from "angular2/core";
import {RouteConfig, Route, RouterOutlet, RouterLink, Router, Location} from "angular2/router";
import {LangService} from "../core/services/LangService";

@Component({
	selector: "language-selection",
	templateUrl: "modules/language-selection.template.html",
	directives: [RouterOutlet, RouterLink]
})
export class LanguageSelection {
	private language: string;
	router: Router;
	languages: Array<any>;

	constructor(private _langService: LangService, router: Router) {
		this.router = router;
		this.language = _langService.language$;

		this.languages = [
			{
				lang: "en",
				button: "en/button-lang.svg"
			},
			{
				lang: "de",
				button: "de/button-lang.svg"
			},
			{
				lang: "es",
				button: "es/button-lang.svg"
			},
			{
				lang: "fr",
				button: "fr/button-lang.svg"
			},
			{
				lang: "it",
				button: "it/button-lang.svg"
			},
			{
				lang: "ja",
				button: "ja/button-lang.svg"
			},
			{
				lang: "ko",
				button: "ko/button-lang.svg"
			},
			{
				lang: "nl",
				button: "nl/button-lang.svg"
			},
			{
				lang: "pt",
				button: "pt/button-lang.svg"
			},
			{
				lang: "tr",
				button: "tr/button-lang.svg"
			},
			{
				lang: "zh",
				button: "zh/button-lang.svg"
			}
		];
	}

	ngOnInit() {
		this._langService.language$.subscribe(currentLanguage => {
			this.language = currentLanguage;
		});
		this._langService.load();
	}

	switchLanguage(lang) {
		this._langService.change(lang);
		this.router.navigate(["/Home"]);
	}
}
