"use strict";

// import Angular 2
import {Component} from "angular2/core";
import {Router} from "angular2/router";
import {PremiumService} from "../core/services/PremiumService";

@Component({
	selector: "menu-purchase",
	templateUrl: "modules/menu-purchase.template.html"
})

export class Purchase {
	private premium: boolean;
	router: Router;

	constructor(private _PremiumService: PremiumService, router: Router) {
		this.router = router;
		this.premium = _PremiumService.premium$;
	}

	ngOnInit() {
		this._PremiumService.premium$.subscribe(
			premiumStatus => {
				this.premium = premiumStatus;
		});
		this._PremiumService.load();
	}

	purchase() {
		this._PremiumService.purchase();
		this.router.navigate(["/Home"]);
	}
}
