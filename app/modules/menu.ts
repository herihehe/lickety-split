import {Component, View, Input} from "angular2/core";
import {NgFor} from "angular2/common";
import {Router, RouterLink} from "angular2/router";
import {LangService} from "../core/services/LangService";
import {PremiumService} from "../core/services/PremiumService";


@Component({
	selector: "menu"
})

@View({
	template: `
		<div class="row row-button">
			<div class="col-xs-6" *ngFor="#menu of menus">
				<a (click)="handleMenu(menu)" [class.inactive]="menu.premium && !purchased" class="u-block"><img src="images/{{language}}/{{menu.button}}" alt="" class="image-svg"></a>
			</div>
		</div>
	`,
	directives: [NgFor, RouterLink]
})

export class Menu {
	@Input() menus;
	private language: string;
	private purchased: boolean;
	router: Router;
	constructor(
		private _LangService: LangService, 
		private _PremiumService: PremiumService,
		router: Router){
		this.router = router;
	}

	ngOnInit() {
		this._LangService.language$.subscribe(
			currentLanguage => {
				this.language = currentLanguage;
		});
		this._LangService.load();

		this._PremiumService.premium$.subscribe(
			purchased => {
				this.purchased = purchased;
		});
		this._PremiumService.load();
	}

	handleMenu(menu){
		var redirect = menu.premium && !this.purchased ? ["/Purchase"] : menu.path;
		this.router.navigate(redirect);
	}
}
